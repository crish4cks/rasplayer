For more info about this project, please, have a look at [here](http://www.diegm.uniud.it/bernardini/Laboratorio_Didattico/2013-2014/2015-cassa-PTU/upnp_mr.html) or into the /www folder.


HOW TO INSTALL APACHE ANT (on Debian based distros)
sudo apt-get update
sudo apt-get install ant


ANT USAGE
Compile the sources and create the .jar executable:
ant -buildfile build.xml (or simply ant dist)

Run the .jar (Ctrl+C to terminate the execution):
ant run

Clean up:
ant clean


NOTES
If you use Ant, under Linux you don't need to add/change anything in your ~/.bashrc file! ;-)
Instead, if you use the traditional way you've to add all the .jar dependencies to your Java
Classpath.
