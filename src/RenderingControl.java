/*
 * This class implements the Rendering Control Service (RCS) and
 * provides basic actions in order to control the volume for the
 * audio track.
 * 
 *
*/

import java.io.*;
import java.lang.Math.*;

import org.cybergarage.upnp.Service;

public class RenderingControl {
    
    private Service service;
    private static final int MAX_VOLUME = 100;
    private static final int MIN_VOLUME = 0;
    private static final int STEP_VOLUME = 1;

    private int Volume;
    private int PreviousVolume;
    private boolean Mute;
    
    
    // Default constructor
    RenderingControl() {        
        // 'PCM' is the audio channel provided by ALSA onto the Raspberry Pi.
        // In most cases you find 'Master'.
        // InstanceID will be kept as "0" becuase we suppose that the Control
        // Point is able to manage one instance of a Media Renderer.
        this.setVolume("InstanceID", "PCM", 60); 
        PreviousVolume = Volume;        
    }
    
    
    // Just another constructor... 
    RenderingControl(String InstanceID, String channel, int DesiredVolume) {                
        this.setVolume(InstanceID, channel, DesiredVolume);
        PreviousVolume = Volume;        
    }    
    
    
    // This method tries to improve the volume scale. It works fine but surely there's a better way
    // to do this.
    // y = m*(x/a) + q with m = 1
    // We want to find a and q in order to have PCM to be set to 60 when the user sets the volume to 5
    // and 100 when the user sets 100
    private int encodeVolume(int value) {
        if(value == 0)
            return 0;
        else return Math.round(0.421f * value + 57.89f);
    }
    
    
    // The inverse function (for the above method)
    private int decodeVolume(int value) {
        if(value == 0)
            return 0;
        else return Math.round(2.375f * (value - 57.89f));
    }
    
    
    // Sets the audio mixer using ALSA
    private void setAlsaMixer(String InstanceID, String channel, int DesiredVolume) {        
        try {
            Runtime.getRuntime().exec("amixer set " + channel + " " + Volume + "%");
        }
        catch(IOException e) {
            System.out.println("Exception: " + e.getMessage());    
            System.exit(-1);
        }
        return;
    }
    
    
    // Checks the range validity for the volume
    private int validate(int DesiredVolume) {
        if(DesiredVolume < MIN_VOLUME)
            return MIN_VOLUME;
        else if(DesiredVolume > MAX_VOLUME)
                 return MAX_VOLUME;
        else return DesiredVolume;
    }
    
        
    //  This method retrieves the current value of the "Volume" state variable of the specified channel for the
    //  specified instance of this service. The CurrentVolume (OUT) parameter contains a value ranging from 
    //  MIN_VOLUME to MAX_VOLUME.
    public int getVolume(String InstanceID, String channel) {
        return decodeVolume(Volume);
    }
        
      
    // Sets the "Volume" state variable of the specified Instance and Channel to the specified value. The
    // "DesiredVolume" input parameter contains a value ranging from MIN_VOLUME to MAX_VOLUME.    
    public void setVolume(String InstanceID, String channel, int DesiredVolume) {
        Volume = validate(encodeVolume(DesiredVolume));
        setAlsaMixer(InstanceID, channel, Volume);
        if(DesiredVolume == 0)
            Mute = true;
        else Mute = false;    
    }
    
    
    // Additional method...
    public void increaseVolume(String InstanceID, String channel) {        
        setVolume(InstanceID, channel, Volume + STEP_VOLUME);
    }
  
  
    // Additional method...
    public void decreaseVolume(String InstanceID, String channel) {
        setVolume(InstanceID, channel, Volume - STEP_VOLUME);
    }
  
  
    // Retrieves the current value of the "Mute" setting of the channel 
    // for the specified instance of this service.
    public boolean getMute(String InstanceID, String channel) {
        return Mute;    
    }
    
    
    // Sets the "Mute" state variable of the specified instance 
    // of this service to the specified value.
    public void setMute(String InstanceID, String channel, boolean DesiredMute) {
        Mute = DesiredMute;        
        if(Mute == true) {
            PreviousVolume = Volume;
            Volume = MIN_VOLUME;        
            setAlsaMixer(InstanceID, channel, Volume);
        }
        else {
            Volume = PreviousVolume;        
            setAlsaMixer(InstanceID, channel, Volume);
        }
    }   
    
    // Binds the service to the device (see the main class)
    public void setService(Service srv) {        
        this.service = srv;
    }
} 
