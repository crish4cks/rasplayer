/*
 *  Media Renderer main class
 *
 */

import java.io.*;
import java.util.*; // for calendar object
import java.net.URL;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;


import org.cybergarage.upnp.*;
import org.cybergarage.upnp.UPnP;
import org.cybergarage.upnp.device.*;
import org.cybergarage.upnp.control.*;
import org.cybergarage.upnp.event.*;
import org.cybergarage.net.HostInterface;
import org.cybergarage.http.HTTPRequest;
import org.cybergarage.http.HTTPResponse;
import org.cybergarage.http.HTTPStatus;
import org.cybergarage.upnp.ssdp.SSDPPacket;
import org.cybergarage.upnp.ssdp.SSDPSearchResponse;
import org.cybergarage.upnp.ssdp.SSDPSearchResponseSocket;


import org.json.simple.*;


public class Rasplayer extends Device implements ActionListener {
    
    // Device description relative path
    private final static String DEVICE_DESCRIPTION_URI = "description/description.xml";
    
    // For more details see the HTTP section below 
    private final static String DEVICE_STATUS_URI = "/status";
    private final static String PRESENTATION_URI = "/presentation";    
    private final static String WIFI_INFO_URI = "/wifi_info";
    private final static String WIFI_SETTINGS_URI = "/wifi_settings";
    private final static String SHUTDOWN_URI = "/shutdown";
    private final static String REBOOT_URI = "/reboot";
    
    // RenderingControl and AVTransport objects declaration
    private RenderingControl rngCtrl;
    private AVTransport avTrsp;
    
    // RenederingControl and AVTransport services declaration
    private Service renderingControl;
    private Service avTransport;    
    
    
    public Rasplayer() throws InvalidDescriptionException, IOException {
    
        // Load device description
        super(new File(DEVICE_DESCRIPTION_URI));
        
        // Only IPv4 addresses
        UPnP.setEnable(UPnP.USE_ONLY_IPV4_ADDR);
        
        // Get the IP address to associate to SSDP and HTTP services
        setSSDPBindAddress(HostInterface.getInetAddress(HostInterface.IPV4_BITMASK, null));
        setHTTPBindAddress(HostInterface.getInetAddress(HostInterface.IPV4_BITMASK, null));
        
        // Get the available services
        ServiceList serviceList = getServiceList();
                
        renderingControl = serviceList.getService(0);
        avTransport = serviceList.getService(1);
        
        // The action listener accepts SOAP requests containing the UPnP actions to be performed
        setActionListener(this);                        
        
        // The time (in seconds) to wait before the UPnP Control Point renews the subscription to the services
        // Default is 1800.
        setLeaseTime(300);
        
        setWirelessMode(true);
        
        // Create an empty file "time.txt", used to retrieve the mpg123 playback time
        File fp = new File("./time.txt");    
        try {
            fp.createNewFile();
        }
        catch(IOException e) {
            System.out.println("Error while creating a 'time.txt':" + e.getMessage());
        }
    
    }
    
    
    // Binds the RenderingControl service to a new RenderingControl object
    public void setRenderingControlService(RenderingControl renderingcontrol) {
       rngCtrl= renderingcontrol;
       rngCtrl.setService(renderingControl); 
            
    }
    
    
    // Binds the AVTransport service to a new AVTransport object
    public void setAVTransportService(AVTransport avtransport) {
       avTrsp = avtransport;
       avTrsp.setService(avTransport); 
    }
    
    
    /////////////////////
    // Control Section //
    /////////////////////
    
    public boolean actionControlReceived(Action action) {
        
        String actionName = action.getName();
        
        
        //////////////////////////////        
        // RenderingControl actions //
        //////////////////////////////
        
        if(actionName.equals("GetMute") == true) { 
            Argument InstanceID = action.getArgument("InstanceID");
            Argument channel = action.getArgument("Channel");
            boolean mute = rngCtrl.getMute(InstanceID.getValue(), channel.getValue());
            action.setArgumentValue("CurrentMute", mute + "");            
            return true;
        }
        
        if(actionName.equals("SetMute") == true) { 
            Argument InstanceID = action.getArgument("InstanceID");
            Argument channel = action.getArgument("Channel");
            Argument desiredMute = action.getArgument("DesiredMute");
            rngCtrl.setMute(InstanceID.getValue(), channel.getValue(), Boolean.parseBoolean(desiredMute.getValue().toLowerCase()));
            return true;
        }
        
        if(actionName.equals("GetVolume") == true) { 
            Argument InstanceID = action.getArgument("InstanceID");
            Argument channel = action.getArgument("Channel");            
            int volume = rngCtrl.getVolume(InstanceID.getValue(), channel.getValue());
            action.setArgumentValue("CurrentVolume", volume);
            return true;
        }
        
        if(actionName.equals("SetVolume") == true) { 
            Argument InstanceID = action.getArgument("InstanceID");
            Argument channel = action.getArgument("Channel");
            Argument desiredVolume = action.getArgument("DesiredVolume");
            rngCtrl.setVolume(InstanceID.getValue(), channel.getValue(), Integer.parseInt(desiredVolume.getValue()));
            return true;
        }
        
        if(actionName.equals("IncreaseVolume") == true) { 
            Argument InstanceID = action.getArgument("InstanceID");
            Argument channel = action.getArgument("Channel");            
            rngCtrl.increaseVolume(InstanceID.getValue(), channel.getValue());
            return true;
        }
        
        if(actionName.equals("DecreaseVolume") == true) { 
            Argument InstanceID = action.getArgument("InstanceID");
            Argument channel = action.getArgument("Channel");            
            rngCtrl.decreaseVolume(InstanceID.getValue(), channel.getValue());
            return true;
        }
        
        
        //////////////////////////
        // AVTtransport actions //
        //////////////////////////
        
        if (actionName.equals("GetTransportInfo") == true) {
            Argument InstanceID = action.getArgument("InstanceID");
            String[] values = avTrsp.getTransportInfo(InstanceID.getValue());
            action.setArgumentValue("CurrentTransportState", values[0]);
            action.setArgumentValue("CurrentTransportStatus", values[1]);
            action.setArgumentValue("CurrentSpeed", values[2]);
            return true;
        }
        
        if (actionName.equals("GetPositionInfo") == true) {
            Argument InstanceID = action.getArgument("InstanceID");
            String[] values = avTrsp.getPositionInfo(InstanceID.getValue());
            action.setArgumentValue("Track", values[0]);
            action.setArgumentValue("TrackDuration", values[1]);
            action.setArgumentValue("TrackMetaData", values[2]);
            action.setArgumentValue("TrackURI", values[3]);
            action.setArgumentValue("RelTime", values[4]);
            action.setArgumentValue("AbsTime", values[5]);
            action.setArgumentValue("RelCount", values[6]);
            action.setArgumentValue("AbsCount", values[7]);
            return true;
        }
        
        if(actionName.equals("SetAVTransportURI") == true) { 
            Argument InstanceID = action.getArgument("InstanceID");
            Argument avtransportUri = action.getArgument("CurrentURI");
            Argument avtransportUriMetaData = action.getArgument("CurrentURIMetaData");                        
            avTrsp.setAVTransportURI(InstanceID.getValue(), avtransportUri.getValue(), avtransportUriMetaData.getValue());
            return true;
        }
        
        if(actionName.equals("Play") == true) {
            Argument InstanceID = action.getArgument("InstanceID");
            Argument speed = action.getArgument("Speed");
            avTrsp.play(InstanceID.getValue(), speed.getValue());
            return true;
        }
        
        if(actionName.equals("Seek") == true) {
            Argument InstanceID = action.getArgument("InstanceID");
            Argument unit = action.getArgument("Unit");
            Argument target = action.getArgument("Target");
            avTrsp.seek(InstanceID.getValue(), unit.getValue(), target.getValue());
            return true;
        }
        
        if(actionName.equals("Pause") == true) {
            Argument InstanceID = action.getArgument("InstanceID");            
            avTrsp.pause(InstanceID.getValue());
            return true;
        }
        
        if(actionName.equals("Stop") == true) {
            Argument InstanceID = action.getArgument("InstanceID");            
            avTrsp.stop(InstanceID.getValue());
            return true;
        }
        
        return false;       
    }
    
    
    
    //////////////////
    // SSDP Section //
    //////////////////
        
    private void printSSDPRequest(SSDPPacket ssdpPacket) {
        System.out.println("============================================================");
        System.out.println("Incoming SSDP search request:");
        System.out.println("------------------------------------------------------------\n\n");
        System.out.println(ssdpPacket);
        System.out.println("============================================================\n\n\n\n\n");
        return;
    }
    
    
    private void printSSDPSearchResponse(SSDPSearchResponse ssdpResponse) {
        System.out.println("================================================================");
        System.out.println("Outgoing SSDP search response:");
        System.out.println("----------------------------------------------------------------\n\n");
        ssdpResponse.print();
        System.out.println("================================================================\n\n\n\n\n");
        return;
    }
    
    
    // Generates an SSDP response and sends it starting from the incoming SSDP packet
    // It also calls the 'print' method to print the response on the std out
    public boolean postSearchResponse(SSDPPacket ssdpPacket, String st, String usn) {
        String localAddress = ssdpPacket.getLocalAddress();        
        SSDPSearchResponse ssdpResponse = new SSDPSearchResponse();
        ssdpResponse.setLeaseTime(getLeaseTime());
        ssdpResponse.setDate(Calendar.getInstance());
        ssdpResponse.setST(st);
        ssdpResponse.setUSN(usn);        
        ssdpResponse.setLocation(this.getLocationURL(localAddress));                
        String remoteAddress = ssdpPacket.getRemoteAddress();
        int remotePort = ssdpPacket.getRemotePort();
        SSDPSearchResponseSocket ssdpResponseSocket = new SSDPSearchResponseSocket();        
        printSSDPSearchResponse(ssdpResponse);
        int ssdpCount = getSSDPAnnounceCount();
        for (int i = 0; i < ssdpCount; i++) {
            ssdpResponseSocket.post(remoteAddress, remotePort, ssdpResponse);
        }
        return true;
    }
    
    
    // Retrieves the right informations in order to compose the SSDP Search response
    public void deviceSearchResponse(SSDPPacket ssdpPacket) {
        String ssdpST = ssdpPacket.getST();        
        if(ssdpST == null)
            return;
        boolean isRootDevice = isRootDevice();
        String devUSN = getUDN();        
        if(ST.isAllDevice(ssdpST) == true) { // ssdp:all
            if(isRootDevice == true)
                devUSN += "::" + USN.ROOTDEVICE;            
            String devNT = getUDN();
            int repeatCounter = (isRootDevice == true) ? 3 : 2; // send the response 3 times if the device is a root device
            for(int i = 0; i < repeatCounter; i++)
                postSearchResponse(ssdpPacket, ssdpST, devUSN );
        }        
        else if(ST.isRootDevice(ssdpST) == true) { // upnp:rootdevice
            if(isRootDevice == true)
                postSearchResponse(ssdpPacket, ssdpST, devUSN + "::" + USN.ROOTDEVICE);
        }   
        
        // This part is ok but in our case we only have a root device
        ServiceList serviceList = getServiceList();
        int serviceCounted = serviceList.size();
        for(int i = 0; i < serviceCounted; i++) {
            Service service = serviceList.getService(i);
            service.serviceSearchResponse(ssdpPacket);
        }
        
        DeviceList childDeviceList = getDeviceList();
        int childDeviceCounted = childDeviceList.size();
        for(int i = 0; i < childDeviceCounted; i++) {
            Device childDevice = childDeviceList.getDevice(i);
            childDevice.deviceSearchResponse(ssdpPacket);
        }
    }
    
    
    // SSDP Search request handler
    public void deviceSearchReceived(SSDPPacket ssdpPacket) {
        printSSDPRequest(ssdpPacket);
        deviceSearchResponse(ssdpPacket);
    }
   
   
    //////////////////
    // HTTP Section //
    //////////////////
    
    private void printHttpRequest(HTTPRequest httpRequest) {
        System.out.println("============================================================");
        System.out.println("Incoming HTTP request:");
        System.out.println("------------------------------------------------------------\n\n");
        httpRequest.print();
        System.out.println("============================================================\n\n\n\n\n");
        return;
    }
   
   
    private void printHttpResponse(HTTPResponse httpResponse) {
        System.out.println("============================================================");
        System.out.println("Outgoing HTTP response:");
        System.out.println("------------------------------------------------------------\n\n");
        httpResponse.print();
        System.out.println("============================================================\n\n\n\n\n");
        return;
    }
   
   
    // This big method manages all the HTTP requests coming from the 
    // UPnP Control Point and from the Web interface
    @SuppressWarnings("unchecked")
    public void httpRequestRecieved(HTTPRequest httpRequest) {              
        printHttpRequest(httpRequest);        
        
        // Halts the Raspberry Pi and sends an HTTP response
        // when you perform an HTTP GET request to the address
        // http://DEVICE_IP:DEVICE_PORT/shutdown via the Web interface.
        // (DEVICE_PORT is left to 4004 by default)
        if(httpRequest.getURI().startsWith(SHUTDOWN_URI)) {
            JSONObject obj = new JSONObject();
            obj.put("MSG", "Speakers will suhtdown now!");
            try {
                Runtime.getRuntime().exec("sudo bash raspman.sh turn_off");                            
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }
            HTTPResponse httpResponse = new HTTPResponse();
            httpResponse.setStatusCode(HTTPStatus.OK);
            httpResponse.setHeader("Access-Control-Allow-Origin","*");
            httpResponse.setContentType("application/json");
            httpResponse.setContent(JSONValue.toJSONString(obj));
            httpRequest.post(httpResponse);
            printHttpResponse(httpResponse);
            return;
        }
        
        // Reboots the Raspberry Pi and sends an HTTP response
        // when you perform an HTTP GET request to the address
        // http://DEVICE_IP:DEVICE_PORT/reboot via the Web interface.
        // (DEVICE_PORT is left to 4004 by default)
        if(httpRequest.getURI().startsWith(REBOOT_URI)) {
            JSONObject obj = new JSONObject();
            obj.put("MSG", "Speakers will reboot now!");
            try {
                Runtime.getRuntime().exec("sudo bash raspman.sh restart");                            
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }
            HTTPResponse httpResponse = new HTTPResponse();
            httpResponse.setStatusCode(HTTPStatus.OK);
            httpResponse.setHeader("Access-Control-Allow-Origin","*");
            httpResponse.setContentType("application/json");
            httpResponse.setContent(JSONValue.toJSONString(obj));
            httpRequest.post(httpResponse);
            printHttpResponse(httpResponse);
            return;
        }
        
        // Sends a JSON object containing the data concering
        // the WiFi networks scan when an HTTP GET request is 
        // performed to the address http://DEVICE_IP:DEVICE_PORT/wifi_info.
        // The GET request is performed automatically when you open
        // the Web interface on a browser
        if(httpRequest.getURI().startsWith(WIFI_INFO_URI)) {
            TimeUtils time = new TimeUtils();
            try {
                Runtime.getRuntime().exec("sudo bash raspman.sh wifi_info");
                time.delay(3000);
                System.out.println("WiFi networks available: " + getWiFiNetNum());                
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }                        
            
            HTTPResponse httpResponse = new HTTPResponse();
            httpResponse.setStatusCode(HTTPStatus.OK);
            httpResponse.setHeader("Access-Control-Allow-Origin","*");
            httpResponse.setContentType("application/json");               
            try {
                httpResponse.setContent(getWiFiInfo());
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }            
            httpRequest.post(httpResponse);            
            printHttpResponse(httpResponse);
            return;
        }        
        
        // Creates the new WiFi network configuration file and places it
        // in the right place when the user submits the data through the 
        // Web interface.
        // This context is invoked when an HTTP GET request is performed
        // in this way:
        // http://DEVICE_IP:DEVICE_PORT/wifi_settings?ESSID=xxx&ENCTYPE=yyy&PWD=zzz
        if(httpRequest.getURI().startsWith(WIFI_SETTINGS_URI)) {
            String[] settings = getWiFiSettings(httpRequest.getFirstLineString());
            setWiFiSettings(settings);
            try {
                Runtime.getRuntime().exec("sudo bash raspman.sh wifi_settings");                            
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }
            
            HTTPResponse httpResponse = new HTTPResponse();
            httpResponse.setStatusCode(HTTPStatus.OK);            
            httpResponse.setHeader("Access-Control-Allow-Origin","*");
            httpResponse.setContent("Wi-Fi settings changed successfully! Speakers will reboot now!");
            httpRequest.post(httpResponse);
            printHttpResponse(httpResponse);
            return;        
        }
        
        
        // Sends an HTTP response containing the status of the device.
        // In particular it says if the UPnP Control Point is currently subscribed 
        // or not subscribed to the AVTRansport and the RenderingControl services.
        // I tested it by performing some HTTP POST requests as this one below:
        // curl --data "avtSID=VALUE&rngSID=VALUE" http://DEVICE_IP:DEVICE_PORT/status
        // NOTE: SID stays for Subscription Identifier (it must be unique)
        if(httpRequest.getURI().startsWith(DEVICE_STATUS_URI)) {
            SubscriberList avTransportSubList = new SubscriberList();
            SubscriberList renderingControlSubList = new SubscriberList();                                    
            avTransportSubList = avTransport.getSubscriberList();
            renderingControlSubList = renderingControl.getSubscriberList();
                        
            String avtSID = httpRequest.getParameterValue("avtSID");            
            String rngSID = httpRequest.getParameterValue("rngSID");            
            String avtStatus = new String("NOT_SUBSCRIBED");
            String rngStatus = new String("NOT_SUBSCRIBED");                       
            
            for(int i = 0; i < avTransportSubList.size(); i++) {                
                if(avtSID.equals(avTransportSubList.getSubscriber(i).getSID()) && !avTransportSubList.getSubscriber(i).isExpired()) {
                    avtStatus = new String("OK");                    
                    break;
                }    
            }
            
            for(int i = 0; i < renderingControlSubList.size(); i++) {                
                if(rngSID.equals(renderingControlSubList.getSubscriber(i).getSID()) && !renderingControlSubList.getSubscriber(i).isExpired()) {
                    rngStatus = new String("OK");                    
                    break;
                }
            }            
                                               
            HTTPResponse httpResponse = new HTTPResponse();
            httpResponse.setStatusCode(HTTPStatus.OK);
            httpResponse.setHeader("Access-Control-Allow-Origin","*");
            httpResponse.setContentType("text/plain");
            httpResponse.setContent("AVTransport: " + avtStatus + "\n" + "RenderingControl: " + rngStatus + "\n");
            httpRequest.post(httpResponse);
            printHttpResponse(httpResponse);
            return;
        }        
        
        
        // The presentation part is not used (does nothing)
        if(httpRequest.getURI().startsWith(PRESENTATION_URI) == false) {
            super.httpRequestRecieved(httpRequest);
            return;
        }           
        
    }             
     
    
    
    // Finds the number of WiFi networks available by analyzing
    // the content of "wifi_info.dat" generated by the bash script
    // "raspman.sh" which is called by the Media Renderer Web interface
    private int getWiFiNetNum() throws IOException {
        File fpin = new File("wifi_info.dat");
        BufferedReader b = new BufferedReader(new FileReader(fpin));       
        String str = null;
        int n = 0;
        while((str = b.readLine()) != null) {
            if(str.indexOf("ESSID") != -1)
                n++;            
        }
        b.close();
        return n; 
    }
    
    
    // Returns  a stringified JSON object containing the informations of a WiFi network scan
    // obtained by analyzing the content of the file "wifi_info.dat" generated by the bash 
    // script "raspman.sh" which is called by the UPnP Media Renderer Web interface
    // This is a possible example of output (two WiFi networks have been found):
    // [{"ID":0,"ESSID":"netgear","QUALITY":96,"ENCTYPE":"WPA2","FREQ":2.462,"CHANNEL":11},
    //  {"ID":1,"ESSID":"WLANCSIT","QUALITY":65,"ENCTYPE":"WPA2","FREQ":2.412,"CHANNEL":1}]
    @SuppressWarnings("unchecked")
    private String getWiFiInfo() throws IOException {
        File fpin = new File("wifi_info.dat");
        JSONArray netList = new JSONArray();       
        JSONObject obj = new JSONObject();
        BufferedReader b = new BufferedReader(new FileReader(fpin));       
        String str = null;
        String essid = null;
        String enctype = null;                    
        float quality = 0.0F;
        float quality_format = 0.0F;
        int idx, id = 0;
        int channel = -1;
        float freq = 0.0F;
        boolean essid_chk = false;
        
        
        while((str = b.readLine()) != null) {
            if(!essid_chk)
                if((idx = str.indexOf("ESSID:")) != -1) {
                    essid = str.substring(idx + 7, str.lastIndexOf('"'));                                
                    essid_chk = true;
                }
                
            if((idx = str.indexOf("Frequency:")) != -1) {
                freq = Float.parseFloat(str.substring(idx + 10, str.indexOf("GHz") - 1));
                if((idx = str.indexOf("Channel")) != -1)
                    channel = Integer.parseInt(str.substring(idx + 8, str.lastIndexOf(')')));                
            }
            
            if(str.indexOf("Encryption key:off") != -1)
                enctype = "Open";                  
                
            if(str.indexOf("IE: IEEE") != -1) {
                enctype = "WEP";
                if(str.indexOf("WPA") != -1)
                    enctype = "WPA";                
                if(str.indexOf("WPA2") != -1)
                        enctype = "WPA2";                                                
            }            
            
            if((idx = str.indexOf("Quality")) != -1) {
                quality_format = Float.parseFloat(str.substring(str.indexOf('/') + 1, str.indexOf('/') + 4).trim());                
                quality = Float.parseFloat(str.substring(idx + 8, str.indexOf('/')));                                
                quality = (quality / quality_format) * 100;                 
            }
            
            if((enctype == "Open" || enctype == "WEP" || enctype == "WPA" || enctype == "WPA2") && quality != 0.0F && freq != 0.0F && channel != -1) {
                netList.add(new WiFiInfo(id, essid, Math.round(quality), enctype, freq, channel));
                enctype = null;      
                essid_chk = false;   
                quality = 0.0F;
                try {
                    if(id == getWiFiNetNum() - 1)
                        break;
                }
                catch(IOException e) {
                    System.out.println("Exception: " + e.getMessage());    
                    System.exit(-1);
                }
                id++;
            }            
        }
        b.close();
        obj.put("\"" + "WiFiNetworks" + "\"", netList);        
        return JSONValue.toJSONString(netList);
    }
    
    
    // Returns an array with respectively the ESSID, the encryption type
    // and the password chosen by the user for his/her WiFi network
    private String[] getWiFiSettings(String str) {
        String[] settings = new String[3];
        int idx;
        
        idx = str.indexOf("ESSID=");
        settings[0] = str.substring(idx + 6, str.indexOf('&'));
        idx = str.indexOf("ENCTYPE=");
        settings[1] = str.substring(idx + 8, str.lastIndexOf('&'));
        idx = str.indexOf("PWD=");
        settings[2] = str.substring(idx + 4, str.indexOf("HTTP") - 1);
        return settings;
    }
    
    
    // Calls the right method depending on the wireless encryption type
    private void setWiFiSettings(String[] settings) {        
        switch(settings[1]) {
            case "Open": try {
                             setOpen(settings[0]);
                         }
                         catch(IOException e) {
                            System.out.println("Exception: " + e.getMessage());    
                            System.exit(-1);
                         }
                         break;
                         
            case "WEP": try {
                             setWEP(settings);
                         }
                         catch(IOException e) {
                            System.out.println("Exception: " + e.getMessage());    
                            System.exit(-1);
                         }
                        break;
                        
            case "WPA": try {
                             setWPA(settings);
                         }
                         catch(IOException e) {
                            System.out.println("Exception: " + e.getMessage());    
                            System.exit(-1);
                         }
                        break;
                        
            case "WPA2": try {
                             setWPA(settings);
                         }
                         catch(IOException e) {
                            System.out.println("Exception: " + e.getMessage());    
                            System.exit(-1);
                         }
                         break;                                 
        }                         
        return;
    }
    
    
    // Creates a network configuration file for a free wireless network
    private void setOpen(String essid) throws IOException {
        File fpin = new File("./interface/models/interfaces.opn");
        PrintWriter out = new PrintWriter("interfaces.dat");
        BufferedReader b = new BufferedReader(new FileReader(fpin));       
        String str = null;        
        while((str = b.readLine()) != null) {
            if(str.indexOf("ESSID") != -1)
                out.println("    wireless-essid " + essid);            
            else out.println(str);
        }
        b.close();  
        out.close();
        return;
    }
    
    
    // Creates a network configuration file for a WEP encrypted wireless network
    private void setWEP(String[] settings) throws IOException {
        File fpin = new File("./interface/models/interfaces.wep");
        PrintWriter out = new PrintWriter("interfaces.dat");
        BufferedReader b = new BufferedReader(new FileReader(fpin));       
        String str = null;        
        while((str = b.readLine()) != null) {
            if(str.indexOf("ESSID") != -1)
                out.println("    wireless-essid " + settings[0]);            
            else if(str.indexOf("PASSWORD") != -1)
                     out.println("    wireless-key s:" + settings[2]);            
                 else out.println(str);
        }
        b.close();  
        out.close();
        return;        
    }
    
    
    // Creates a network configuration file for a WPA/WPA2 encrypted wireless network
    private void setWPA(String[] settings) throws IOException {
        File fpin = new File("./interface/models/interfaces.wpa");
        PrintWriter out = new PrintWriter("interfaces.dat");
        BufferedReader b = new BufferedReader(new FileReader(fpin));       
        String str = null;        
        while((str = b.readLine()) != null) {
            if(str.indexOf("ESSID") != -1)
                out.println("    wpa-ssid " + settings[0]);            
            else if(str.indexOf("PASSWORD") != -1)
                     out.println("    wpa-psk " + settings[2]);            
                 else out.println(str);
        }
        b.close();  
        out.close();
        return;
    }
    
    
    //////////////
    // Main... ///
    //////////////
    
    public static void main(String[] args) {        
        Rasplayer rasp = null;
        try {
            rasp = new Rasplayer();            
        }
        catch(InvalidDescriptionException e) {            
            System.out.println("Exception: " + e.getMessage());
            System.exit(-1);
        }
        catch(IOException e) {
            System.out.println("Exception: " + e.getMessage());
            System.exit(-1);
        }
                
        // Binds the RenderingControl service to a new RenderingControl object
        rasp.setRenderingControlService(new RenderingControl());
        
        // Binds the AVTransport service to a new AVTransport object
        rasp.setAVTransportService(new AVTransport());
        
        // Starts the UPnP Media Renderer and announces it to the network through a notify message using ssdp:alive
        rasp.start();
    }
} 
 