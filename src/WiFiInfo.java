/*
 * A simple JSON encoder used to provide informations
 * related to WiFi network scan.
 *
 */

import java.io.*; 
 
import org.json.simple.*;

public class WiFiInfo implements JSONAware {
    
    private int id;
    private String essid;
    private int quality;
    private String enctype;    
    private float freq;
    private int channel;
    
    // Constructor
    public WiFiInfo(int id, String essid, int quality, String enctype, float freq, int channel) {
        this.id = id;
        this.essid = essid;
        this.quality = quality;
        this.enctype = enctype;
        this.freq = freq;
        this.channel = channel;
        
    }
    
    
    public String toJSONString() {
        // retrieve data from wifi_info.dat...
        StringBuffer sb = new StringBuffer();                
        sb.append("{");                
        sb.append("\"" + JSONObject.escape("ID") + "\"");
        sb.append(":");
        sb.append(id);                
        sb.append(",");                
        sb.append("\"" + JSONObject.escape("ESSID") + "\"");
        sb.append(":");
        sb.append("\"" + JSONObject.escape(essid) + "\"");                
        sb.append(",");                                                        
        sb.append("\"" + JSONObject.escape("QUALITY") + "\"");
        sb.append(":");
        sb.append(quality);
        sb.append(",");
        sb.append("\"" + JSONObject.escape("ENCTYPE") + "\"");
        sb.append(":");
        sb.append("\"" + JSONObject.escape(enctype) + "\"");
        sb.append(",");
        sb.append("\"" + JSONObject.escape("FREQ") + "\"");
        sb.append(":");
        sb.append(freq);
        sb.append(",");
        sb.append("\"" + JSONObject.escape("CHANNEL") + "\"");
        sb.append(":");
        sb.append(channel);        
        sb.append("}");
        return sb.toString();
    }
}
