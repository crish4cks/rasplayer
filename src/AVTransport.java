/*
 * AVTransport service.
 *
 */

import java.io.*;

import org.cybergarage.upnp.Service;


public class AVTransport {

    private String uri;
    private String duration;
    private String trackMetaData;
    private String playbackTime;
    private AudioPlayer mp3player;
    private Service service;
    private TimeUtils time = new TimeUtils();
    
    
    // Constructor
    AVTransport() {        
        this.mp3player = new AudioPlayer("http://");
    }
        
    
    // Extracts the mp3 playback duration from the XML meta data
    // sent from the UPnP Control Point
    private String getDuration(String xmlstr) {
        trackMetaData = xmlstr;
        int idx = xmlstr.indexOf("duration=");        
        return xmlstr.substring(idx + 10, idx + 15);
    }
    
    
    
    // Extracts the current playback time by reading it from
    // time.txt which is continuously updated by mpg123
    // For more details see:
    // http://crish4cks.net/way-extract-playback-time-mpg123/
    public String getPlaybackTime() throws IOException {            
        File fpin = new File("time.txt");
        BufferedReader b = new BufferedReader(new FileReader(fpin));        
        playbackTime = b.readLine();
	b.close();
	System.out.println("PLAYBACK_TIME: " + playbackTime);
	return playbackTime;
    }
    
    
    // Binds the service to the device (see the main class)
    public void setService(Service srv) {        
        this.service = srv;        
    }
    
    
    // Returns a string array with:
    // vales[0]: the state of the renderer
    // values[1]: the status of the renderer
    // values[2]: the playback speed (default is 1)
    public String[] getTransportInfo(String InstanceID) {
        String[] values = new String[3];        
        if(mp3player.getState() != null) {
            values[0] = mp3player.getState();        
            values[1] = "OK";
            values[2] = "1";
        } 
        else {
            values[0] = "";        
            values[1] = "ERROR"; 
            values[2] = "";        
        }
        for(int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
        }        
        return values;
    }
    
    
    // Returns a string array with some informations about the current track
    public String[] getPositionInfo(String InstanceID) {
        String[] values = new String[8];
        try {
            getPlaybackTime();
        }
        catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());    
            System.exit(-1);
        }
        if(time.compareTimes(playbackTime, duration))
            playbackTime = "00:00";
        values[0] = "1";                                                     // Track
        values[1] = duration;                                                // TrackDuration
        values[2] = trackMetaData;                                           // TrackMetaData
        values[3] = uri;                                                     // TrackURI
        values[4] = playbackTime;                                            // RelTime
        values[5] = "NOT_IMPLEMENTED";                                       // AbsTime
        values[6] = Integer.toString(time.getSeconds("00:" + playbackTime)); // RelCount
        values[7] = "NOT_IMPLEMENTED";                                       // AbsCount
        return values;
    }
    
    
    // Sets the URI of the mp3 file by calling the 'setUri' method of the AudioPlayer class.
    // Then extracts and stores the current mp3 playback duration.
    public void setAVTransportURI(String InstanceID, String CurrentURI, String CurrentURIMetaData) {
        uri = CurrentURI;   
        mp3player.setUri(uri);        
        duration = getDuration(CurrentURIMetaData);
        System.out.println("Duration: " + duration);             
    }

   
    // Calls the 'play' method of the AudioPlayer class
    public void play(String InstanceID, String TransportPlaySpeed) {        
        mp3player.play();
    }

    
    // Calls the 'seek' method of the AudioPlayer class
    public void seek(String InstanceID, String unit, String target) {
        mp3player.seek(unit, target);
        return;
    }
    
    
    // Calls the 'stop' method of the AudioPlayer class
    public void stop(String InstanceID) {
        mp3player.stop();        
    }
    

    // Calls the 'pause' method of the AudioPlayer class
    public void pause(String InstanceID) {        
        mp3player.pause();            
    }
} 
