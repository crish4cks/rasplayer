/*
 * Time utilities class.
 *
 */
 

public class TimeUtils {            
    
    // Performs a delay of a given amount of milliseconds
    public static void delay(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } 
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    
    
    // Returns the number of seconds.
    // timeStr is supposed to be in the format hh:mm:ss
    public int getSeconds(String timeStr) {        
        return 3600*Integer.parseInt(timeStr.substring(0,2)) + 60*Integer.parseInt(timeStr.substring(3,5)) + Integer.parseInt(timeStr.substring(6,8));
    }
    
    
    // Returns "true" if fTimeStr is >= sTimeStr, "false" otherwise
    // fTimeStr and sTimeStr are supposed to be in the format mm:ss
    public boolean compareTimes(String fTimeStr, String sTimeStr) {        
        return getSeconds("00:" + fTimeStr) >= getSeconds("00:" + sTimeStr) ? true : false;
    }    
} 
