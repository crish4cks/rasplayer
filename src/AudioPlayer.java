/*
 * Simple mp3 player with <play>, <stop>, <pause> and <seek> actions, based on mpg123.
 * The player retrieves data from a remote server (the UPnP Media Server) through the
 * AVTransport Service.
 *
 */


import java.io.*;


public class AudioPlayer {
    
    private String url;    
    private String playerState;  
    
    // We assume that mp3 files have 1152 samples per frame 
    // and the sampling frequency is 44100 Hz so, the duration
    // of a single frame is about 26 ms (MPEG Layer III, version 1).
    //
    // frame duration (milliseconds per frame) = 
    // (samples per frame/ samples per second) * 1000
    //
    private static final float FPS = 38.46f; // 1/26ms 
    
    private TimeUtils time = new TimeUtils();

    
    // Constructor
    AudioPlayer(String mp3url) {
        this.url = mp3url;    
        this.playerState = "STOPPED";        
    }                
    
    
    // Sets the player's state
    private void setState(String state) {         
        playerState = validateState(state);
        return;
    }    
    
    
    // Checks if the player state is valid
    private String validateState(String state) {
        switch(state.toUpperCase()) {
            case "STOPPED":                 
                break;
            case "PAUSED_PLAYBACK":                 
                break;    
            case "PLAYING":                 
                break;
            default:
                state = "STOPPED";
                break;
        }
        return state;
    }    
    
    
    // Resume (if paused)
    private void resume() {       
        if(playerState == "PAUSED_PLAYBACK") {
            try {
                Runtime.getRuntime().exec("pkill -CONT mpg123");
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }
            setState("PLAYING");
        }        
        return;
    }
    
    
    // Sets the URI
    public void setUri(String uri) {
       url = uri;
       return;
    }
    
    
    // Returns tha player's state
    public String getState() {                 
        return playerState;
    }
    
    
    // Plays the mp3 song...
    public void play() {                
        if(playerState == "PAUSED_PLAYBACK")
            resume();
        else {            
            try {
                if(playerState == "PLAYING")
                    Runtime.getRuntime().exec("pkill -f mpg123");
                if(playerState == "STOPPED")
                    time.delay(300);
                time.delay(300);
                
                // mpg123 params:
                // -v: increases the verbosity level
                // -2: performs a downsampling of ratio 2:1 (22 kHz) on 
                //     the output stream (saves some CPU cycles)
                // -b 2048: uses a 2 MB output buffer
                Runtime.getRuntime().exec("mpg123 -v -2 -b 2048 " + url);
                setState("PLAYING");
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }
        }                             
        return;        
    }
    
    
    // Pauses the player
    public void pause() {        
        if(playerState == "PLAYING") {
            try {
                Runtime.getRuntime().exec("pkill -STOP mpg123");
                setState("PAUSED_PLAYBACK");
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }            
        }
        return;
    }
            
    
    // Stops the player by killing all the processes associated to mpg123
    public void stop() {        
        try {
            Runtime.getRuntime().exec("pkill -f mpg123");
            setState("STOPPED");
        }
        catch(IOException e) {
            System.out.println("Exception: " + e.getMessage());    
            System.exit(-1);
        }        
        return;
    }    
    
    
    // Sets the playback time to the position given by target. 
    // Works on STOP and PLAYING states.
    // unit is always set to "REL_TIME" (the target is an amount 
    // of time relative to the beginning of the current track)
    public void seek(String unit, String target) {
        System.out.println("Player state (before seek main if): " + getState());
        if(playerState == "STOPPED" || playerState == "PLAYING") {            
            try {
                Runtime.getRuntime().exec("pkill -f mpg123");
                setState("STOPPED");
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }
            time.delay(300);
            System.out.println("Player state after stop(): " + getState());
                                        
            try {                                        
                float nFrames = time.getSeconds(target) * FPS;  
                
                // mpg123 params:
                // -k num: skips the first num frames 
                Runtime.getRuntime().exec("mpg123 -v -2 -b 2048 -k " + nFrames + " " + url);
                setState("PLAYING");                    
            }
            catch(IOException e) {
                System.out.println("Exception: " + e.getMessage());    
                System.exit(-1);
            }            
        }        
        return;
    }
}
