/*
 * AVTransport class for testing purposes.
 * AVTransport & RenderingControl tests.
 *
 */

import java.io.*;
import java.net.URL;
import java.net.MalformedURLException;

public class AVTransportTest {                 

    public static void main(String[] args) {
        AVTransport avt = null;
        RenderingControl volumeControl = new RenderingControl();
        TimeUtils time = new TimeUtils();
        
        avt = new AVTransport();
        avt.setAVTransportURI("InstanceID", "http://a.tumblr.com/tumblr_lwze6id6RV1r1idl7o1.mp3", "MetaData"); 
        
        avt.play("InstanceID", "TransportPlaySpeed");
        time.delay(5000);
        volumeControl.setVolume("InstanceID", "PCM", 70);
        time.delay(5000);
        avt.pause("InstanceID");     
        try {
            avt.getPlaybackTime();
        }
        catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());    
            System.exit(-1);
        }
        time.delay(3000);        
        avt.play("InstanceID", "TransportPlaySpeed");        
        time.delay(5000);
        avt.seek("InstanceID", "REL_TIME", "00:01:35");        
        time.delay(5000);
        try {
            avt.getPlaybackTime();
        }
        catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());    
            System.exit(-1);        
        }
        avt.seek("InstanceID", "REL_TIME", "00:03:24");
        time.delay(5000);
        avt.getTransportInfo("InstanceID");
        time.delay(3000);        
        avt.stop("InstanceID");
        return;
    }    
} 
 
