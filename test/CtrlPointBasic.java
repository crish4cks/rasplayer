/*
 * A very basic UPnP control point discovery class for testing purposes.
 *
 */


import java.io.*;

import org.cybergarage.upnp.*;
import org.cybergarage.upnp.device.*;
import org.cybergarage.upnp.control.*;
import org.cybergarage.http.*;
import org.cybergarage.upnp.ssdp.*;

public class CtrlPointBasic extends ControlPoint implements NotifyListener, SearchResponseListener {
    
    private ControlPoint ctrlPoint = new ControlPoint();
    
    public CtrlPointBasic() {
        ctrlPoint.addNotifyListener(this);
        ctrlPoint.addSearchResponseListener(this);
        ctrlPoint.start();
        ctrlPoint.search();        
    }
    
    
    public void deviceNotifyReceived(SSDPPacket ssdpPacket) { 
       
    }
    
    
    public void deviceSearchResponseReceived(SSDPPacket ssdpPacket) {        
        System.out.println("================================================================");
        System.out.println("Incoming SSDP search response:");
        System.out.println("----------------------------------------------------------------\n\n");
        System.out.println(ssdpPacket);
        System.out.println("================================================================\n\n\n\n\n");        
    }
    
    
    public static void main(String[] args) {    
        CtrlPointBasic ctrlPointDev = new CtrlPointBasic();
        Object keepAlive = new Object();
        synchronized(keepAlive) {
            try {
                keepAlive.wait();
            }
            catch(java.lang.InterruptedException e) {
                System.out.println("Exception " + e.getMessage());
                System.exit(-1);
            }
        }
    }
} 
