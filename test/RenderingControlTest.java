/*
 * RenderingControl class for testing purposes.
 * 
 */

public class RenderingControlTest {    

    public static void main(String[] args) {
        RenderingControl volumeControl = new RenderingControl("InstanceID", "Master", 75);
        TimeUtils time = new TimeUtils();
        
        System.out.println("InitialVolume: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("InitialMute: " + volumeControl.getMute("InstanceID", "Master"));
        
        time.delay(4000);
        
        volumeControl.setVolume("InstanceID", "Master", 50);
        volumeControl.setMute("InstanceID", "Master", true);
        System.out.println("CurrentVolume1: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute1: " + volumeControl.getMute("InstanceID", "Master"));    
        
        time.delay(4000);
        
        volumeControl.setMute("InstanceID", "Master", true);
        volumeControl.setVolume("InstanceID", "Master", 50);        
        System.out.println("CurrentVolume2: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute2: " + volumeControl.getMute("InstanceID", "Master"));
        
        time.delay(4000);
        
        volumeControl.setMute("InstanceID", "Master", true);
        volumeControl.setVolume("InstanceID", "Master", 0);
        System.out.println("CurrentVolume3: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute3: " + volumeControl.getMute("InstanceID", "Master"));
        
        time.delay(4000);
        
        volumeControl.setMute("InstanceID", "Master", false);
        volumeControl.setVolume("InstanceID", "Master", 94);
        System.out.println("CurrentVolume3: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute3: " + volumeControl.getMute("InstanceID", "Master"));
        
        time.delay(2000);
                        
        volumeControl.increaseVolume("InstanceID", "Master");        
        System.out.println("CurrentVolume3: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute3: " + volumeControl.getMute("InstanceID", "Master"));
        
        time.delay(2000);
                        
        volumeControl.decreaseVolume("InstanceID", "Master");        
        System.out.println("CurrentVolume3: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute3: " + volumeControl.getMute("InstanceID", "Master"));
        
    }
} 
