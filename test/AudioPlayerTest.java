/*
 * Audio Player class for testing purposes.
 * Mixed tests with also RenderingControl.
 *
 */

import java.io.*;


public class AudioPlayerTest {         

    public static void main(String[] args) {
        AudioPlayer rasplayer = null;
        RenderingControl volumeControl = new RenderingControl();
        TimeUtils time = new TimeUtils();
        
        rasplayer = new AudioPlayer("http://a.tumblr.com/tumblr_lwze6id6RV1r1idl7o1.mp3");
        
        rasplayer.play();
        System.out.println("Player state: " + rasplayer.getState());
        System.out.println("CurrentVolume: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute: " + volumeControl.getMute("InstanceID", "Master"));
        time.delay(3000);
        volumeControl.setVolume("InstanceID", "Master", 70);
        System.out.println("CurrentVolume: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute: " + volumeControl.getMute("InstanceID", "Master"));
        time.delay(3000);
        volumeControl.setMute("InstanceID", "Master", true);
        System.out.println("CurrentVolume: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute: " + volumeControl.getMute("InstanceID", "Master"));
        time.delay(3000);
        volumeControl.setMute("InstanceID", "Master", false);
        System.out.println("CurrentVolume: " + volumeControl.getVolume("InstanceID", "Master"));
        System.out.println("CurrentMute: " + volumeControl.getMute("InstanceID", "Master"));
        time.delay(3000);
        rasplayer.pause();
        System.out.println("Player state: " + rasplayer.getState());
        time.delay(3000);
        rasplayer.play();
        System.out.println("Player state: " + rasplayer.getState());
        time.delay(5000);
        System.out.println("Seek to 00:01:35");
        System.out.println("Player state: " + rasplayer.getState());
        rasplayer.seek("REL_TIME", "00:01:35");
        time.delay(4000);
        rasplayer.pause();
        System.out.println("Player state: " + rasplayer.getState());
        time.delay(3000);
        rasplayer.play();
        System.out.println("Player state: " + rasplayer.getState());
        volumeControl.setVolume("InstanceID", "Master", 50);
        System.out.println("CurrentVolume: " + volumeControl.getVolume("InstanceID", "Master"));
        time.delay(5000);
        rasplayer.stop();
        System.out.println("Player state: " + rasplayer.getState());
        return;
    }    
} 
