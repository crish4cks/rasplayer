#!/bin/bash
#
# This bash script is called (indirectly) by the Media Renderer's Web interface.
# See the HTTP section in ~/rasplayer/src/Rasplayer.java.
#
# NOTE: In order that this script works fine you need to have the right
#       permissions for your user. In particular you need to have the
#       right configuration in /etc/sudoers to be able to execute
#       root commands.
#

# You must set your own wireless interface
WIFI_INTERFACE="wlan0"


# Clean up and halt the system
function turn_off {
    rm -f wifi_info.dat interfaces.dat
    /sbin/init 0
}


# Clean up and reboot the system
function restart {
    rm -f wifi_info.dat interfaces.dat
    /sbin/init 6
}


# Performs a WiFi network scan and saves the results into a file called wifi_info.dat
function wifi_info {
    iwlist $WIFI_INTERFACE scan > wifi_info.dat
}


# Takes a backup of the previous network settings and updates the configuration
function wifi_settings {
    cp /etc/network/interfaces /etc/network/interfaces.bkp
    mv interfaces.dat /etc/network/interfaces
}


case $1 in
        turn_off)
            turn_off
            ;;

        restart)
            restart
            ;;

        wifi_info)
            wifi_info
            ;;

        wifi_settings)
            wifi_settings && restart
            ;;

        *)
            echo $"Usage: $0 {turn_off|restart|wifi_info|wifi_settings}"
            exit 1
esac
