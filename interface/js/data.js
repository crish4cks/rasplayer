/*
 * A simple script to manage the exchange of data from
 * the user interface (located on the UPnP Control Point 
 * Web server) and the UPnP Media Renderer.
 * In this specific case the exchange of data is related
 * to the speakers (UPnP Media Renderer) WiFi settings.
 * 
 */


var ip_address = "192.168.1.104"; // DEVICE_IP (UPnP Media Renderer) set by the UPnP Control Point
var port_num = "4004";            // DEVICE_PORT (default value)
var data;


$(document).ready(function() {    
  
    
    deleteAllCookies();
    $(".pwd").hide();
  
    // When the interface is loaded retrieves the JSON object containing the data of the WiFi networks scan and populates
    // the HTML <select> element dynamically 
    $.ajax({        
        url: "http://" + ip_address + ":" + port_num + "/wifi_info",
        type: "GET",
        async: false,
        dataType: "json",
        success: function(data) {
                     for(i = 0; i < data.length; i++) {        
                         $("#wifi_networks").append("<option value=" + "\"" + i + "\"" + ">" + "\"" + data[i].ESSID + "\"" + "</option>");   
                         setCookie("wifi_json_data", JSON.stringify(data), 1);                         
                     }                     
                 },
        error: function() {
	    alert("Cannot retrieve wireless data! Please, check your connection.");  	  
	}
    });                  
    
    
    // Creates the 'reboot' and 'shutdown' links dynamically 
    $("#links").append("<li><a id=" + "\"" + "shutdown" + "\"" + "href=" + "\"" + "http://" + ip_address + ":" + port_num + "/shutdown" + "\"" + ">Shutdown</a></li>");   
    $("#links").append("<li><a id=" + "\"" + "reboot" + "\"" + "href=" + "\"" + "http://" + ip_address + ":" + port_num + "/reboot" + "\"" + ">Reboot</a></li>")
    
    
    // Sends a 'shutdown' HTTP request when the 'shutdown' link is clicked
    $("#shutdown").click(function(event) {
        event.preventDefault();	
	if(confirm("Are you sure to shutdown the speakers now?") == true) {	  
            $.ajax({        
                url: "http://" + ip_address + ":" + port_num + "/shutdown",
                type: "GET",
                async: false,        
	        dataType: "json",
                success: function(data) {
                             alert(data.MSG);                     
                         },
                error: function() {
	                   alert("Cannot send shutdown request! Please, check your connection.");  	  
	               } 
            });  
	}	
    });
    
    
    // Sends a 'reboot' HTTP request when the 'reboot' link is clicked
    $("#reboot").click(function(event) {
        event.preventDefault();
	if(confirm("Are you sure to reboot the speakers now?") == true) {
            $.ajax({        
                url: "http://" + ip_address + ":" + port_num + "/reboot",
                type: "GET",
                async: false,        
	        dataType: "json",
                success: function(data) {
                             alert(data.MSG);                     
                         },
                error: function() {
	                   alert("Cannot send reboot request! Please, check your connection.");  	  
	               } 
            });  
	}
    });
    
    
    // When you select a WiFi network the details are shown below 
    $("#wifi_networks").change(function() {             
        data = JSON.parse(getCookie("wifi_json_data"));	
        net_value = $("#wifi_networks").val();
	setCookie("net_value", net_value, 1);	
        if(net_value == "default") {
            $("#essid").html("--");
            $("#quality").html("--");
            $("#enctype").html("--");
	    $("#freq").html("--");
	    $("#channel").html("--");    
	    $(".pwd").hide(); 
        }
        else {
            $("#essid").html("\"" + data[net_value].ESSID + "\"");
            $("#quality").html(data[net_value].QUALITY + "%");
            $("#enctype").html(data[net_value].ENCTYPE);
	    $("#freq").html(data[net_value].FREQ);
	    $("#channel").html(data[net_value].CHANNEL);
            if(data[net_value].ENCTYPE == "Open")
                $(".pwd").hide();
	    else $(".pwd").show();
        }                
    });
    
    
    // If the input data are valid, sends an HTTP request with the new WiFi configuration when you confirm your choices
    $("input[name=submit]").on("click", function(event) {
        event.preventDefault();
      
        net_id = $("#wifi_networks").val();	
	pwd = $("input[name=pwd]").val();
	pwd_confirm = $("input[name=pwd_confirm]").val();
      
        if(net_id == "default") {	    
	    alert("You must choose a Wi-Fi network!");
            $("form").submit(function(e){
                return false;
            });	    
	    location.reload();
	    return;
	}
	else {
	    essid = data[parseInt(getCookie("net_value"))].ESSID;
	    enctype = data[parseInt(getCookie("net_value"))].ENCTYPE;  
	}
	
	if(enctype != "Open") {
	    pattern = /^[a-z0-9]+$/i;
	    if(!pwd && !pwd_confirm) {
	        alert("'Password' and 'Confirm password' fields must not be empty!");
                $("form").submit(function(e){
                    return false;
                });  	    
	        location.reload();
		return;
            }	  
	    else if(pwd != pwd_confirm) {
                alert("The passwords inserted don't match!");
                $("form").submit(function(e){
                    return false;
                });  	    
	        location.reload(); 	    
		return;
	    }
	    else if(pwd.length < 8) {
	        alert("At least 8 alphanumeric characters are required for passwords!");
                $("form").submit(function(e){
                    return false;
                });  	    
	        location.reload();
		return;
	    }
	    else if(!pattern.test(pwd)) {
	        alert("The passwords inserted are not alphanumeric!");
                $("form").submit(function(e){
                    return false;
                });  	    
	        location.reload();
		return;
	    }
	}
	else pwd = "";
	
	if(confirm("Change Wi-Fi speakers settings?") == true) {
            $("input[name=essid]").val(essid);
	    $.ajax({        
                url: "http://" + ip_address + ":" + port_num + "/wifi_settings?ESSID=" + essid + "&ENCTYPE=" + enctype + "&PWD=" + pwd,
                type: "GET",            
                async: false,	                
                success: function(data) {
                             alert(data);			 
                         },
                error: function() {
	            alert("Cannot send Wi-Fi settings to the speakers! Please, check your connection.");  	  
                }
            });	        	
	}
    });                
    
    
    // Reset fields
    $("input[name=reset]").on("click", function() {    
        $("#wifi_networks").val(0);
	$("#essid").html("--");
        $("#quality").html("--");
        $("#enctype").html("--");
	$("#freq").html("--");
	$("#channel").html("--");
	$(".pwd").hide();
    });
    
    
});
