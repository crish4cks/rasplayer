/*
 * 
 * This is a set of functions used to manage cookies.
 * 
 * 
 */





/*
 * 
 * Sets a cookie:
 * name = name of the cookie
 * value = value of the cookie
 * expiredays = number of days before expiration.
 * 
 */
function setCookie(name, value, expiredays) {
    var date = new Date();
    date.setTime(date.getTime() + (expiredays*24*60*60*1000));                         // time is in milliseconds    
    document.cookie = name + "=" + value + "; " + "expires=" + date.toGMTString();
    return;
} 



/*
 * 
 * Gets the value of a cookie.
 * 
 */
function getCookie(name) {
    var cookie_name = name + "=";
    var cookie_array = document.cookie.split(';');
    for(var i = 0; i < cookie_array.length; i++) {
        var c = cookie_array[i].trim();
        if (c.indexOf(cookie_name)==0) return c.substring(cookie_name.length,c.length);
    }
    return "";
} 



/*
 * 
 * Deletes a cookie.
 * 
 */
function deleteCookie(name) {
    document.cookie = name + "=; expires=Sun, 17 Jul 1994 12:30:00 GMT";   // what happened that day? ;-)
    return;
}



/*
 * 
 * Returns the list of all cookies printed onto a string.
 * 
 */
function listAllCookies() {
    var str = "Cookies: " + document.cookie;
    return str;
}



/*
 *
 * Deletes all cookies.
 * 
 */
function deleteAllCookies() {                                        
     var cookie_array = document.cookie.split(';');     
     for(var i = 0; i < cookie_array.length; i++) {    
         var c = cookie_array[i].trim();		 
	 c = c.substring(0, c.length).concat("; expires=Sun, 17 Jul 1994 12:30:00 GMT");	     
	 document.cookie = c;	     	
     }
     return;
}   



/*
 *
 * Returns 'true' if a cookie is set. Otherwise it returns 'false'.
 * 
 */
function isSet(name) {                                               
    if(getCookie(name) != "")
        return true;
    else return false;
}
